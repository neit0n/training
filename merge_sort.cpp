/*
 * Simple merge sort
 * https://vk.com/egormoroz
*/
#include <iostream>
#include <algorithm>
#include <random>
#include <chrono>

using namespace std;

void mergeSort(size_t beg, size_t end, vector<int> &v)
{
    if (end - beg < 2)
	return;
    if (end - beg == 2)
    {
	if (v[beg] > v[beg + 1])
	    swap(v[beg], v[beg + 1]);
	return;
    }
    size_t b1 = beg;
    size_t e1 = beg + (end - beg) / 2;
    size_t b2 = e1;
    size_t e2 = end;
    mergeSort(b1, e1, v);
    mergeSort(b2, e2, v);

    vector<int> b;
    b.reserve(end - beg);
    while (b.size() < end - beg)
    {
	if (b2 >= e2 || (b1 < e1 && v[b1] < v[b2]))
	{
	    b.push_back(v[b1]);
	    ++b1;
	}
	else
	{
	    b.push_back(v[b2]);
	    ++b2;
	}
    }
    for (size_t i = 0; i < b.size(); ++i)
	v[beg + i] = b[i];
}

int main()
{
    //генерируем случайные n чисел
    const size_t n = 200;
    vector<int> v;
    v.reserve(n);
    ios::sync_with_stdio(false);
    for (int i = 0; i < n; ++i)
	v.push_back(i);
    random_device rd;
    auto seed = chrono::system_clock::now().time_since_epoch().count();
    shuffle(v.begin(), v.end(), default_random_engine(seed));

    for (const auto &i : v)
	cout << i << " ";
    cout << endl;
	
    //сортируем
    mergeSort(0, v.size(), v);
    for (const auto &i : v)
	cout << i << " ";
}
