/*
 * Power function.
 * https://vk.com/egormoroz
 */
#include <iostream>
#include <random>

uint64_t power(uint64_t value, uint64_t n)
{
	uint64_t r = 1;
	while (n > 0)
	{
		if (n % 2 == 1)
			r *= value;
		value *= value;
		n /= 2;
	}
	return r;
}

int main()
{
	using namespace std;
	ios::sync_with_stdio(false);
	random_device seeder;
	mt19937 engine(seeder());
	uniform_int_distribution<> gen(1, 28);

	//A small benchmark
	//With -O3 takes < 1 second, cout - bottleneck
	//i5-3470 @ 3.2 GHz, kubuntu 17.04
	for (int i = 0; i < 100000; i++)
	{
		int value = gen(engine);
		cout << power(value, value / 2) << ", ";
	}
	cout << "\n\n\n";
	cout << power(2, sizeof(size_t) * 8) - 1 << " == " << 
		numeric_limits<size_t>::max() << endl;

	return 0;
}
